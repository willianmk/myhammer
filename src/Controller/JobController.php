<?php

namespace MyHammer\Controller;

use MyHammer\Domain\Repository\JobInterface;
use MyHammer\Dto\Job as JobDto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class JobController extends AbstractController
{
    private $repository;

    public function __construct(JobInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create(Request $request)
    {
        $jobData = json_decode($request->getContent(), true);

        try{
            $jobDto = JobDto::fromArray($jobData['job']);
            $this->repository->create($jobDto);
        } catch (\InvalidArgumentException $e) {
            return $this->json(
                [
                    'message' => 'Could not create job: ',
                    'details' => json_decode($e->getMessage(), true),
                    'code' => 'JOB400'
                ],
                400
            );
        } catch (\RuntimeException $e) {
            return $this->json(
                [
                    'message' => 'Error creating job: ' . $e->getMessage(),
                    'code' => 'JOB500'
                ],
                500
            );
        }


        return $this->json($jobDto->toArray(), 201);
    }

    public function list(Request $request)
    {
        $service = $request->query->get('service', '');
        $region = $request->query->get('region', '');
        $user = $request->headers->get('X-User', '');

        $result = $this->repository->findByServiceOrRegion($service, $region, $user);

        if (empty($result)) {
            return $this->json(
                [
                    'message' => 'Jobs not found in 30 days',
                    'code' => 'JOB404'
                ],
                404
            );
        }

        return $this->json($result, 200);
    }
}
