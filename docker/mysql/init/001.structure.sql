CREATE SCHEMA `myhammer` DEFAULT CHARACTER SET utf8;
USE myhammer;

CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `region` (
  `zip_code` int(5) unsigned zerofill NOT NULL COMMENT 'This exercise is considering only German Zip Codes',
  `city` varchar(100) NOT NULL,
  PRIMARY KEY (`zip_code`)
);

CREATE TABLE `job` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` INT UNSIGNED NOT NULL,
  `zip_code` int(5) unsigned zerofill NOT NULL,
  `title` varchar(50) NOT NULL,
  `user` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `service_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_job_x_region_idx` (`zip_code`),
  KEY `fk_job_x_category_idx` (`category_id` ASC),
  CONSTRAINT `fk_job_x_region` FOREIGN KEY (`zip_code`) REFERENCES `region` (`zip_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_job_x_category` FOREIGN KEY (`category_id`) REFERENCES `myhammer`.`category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);