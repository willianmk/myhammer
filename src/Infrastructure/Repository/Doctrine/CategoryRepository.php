<?php

namespace MyHammer\Infrastructure\Repository\Doctrine;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use MyHammer\Domain\Repository\CategoryInterface;
use MyHammer\Domain\Entity\CategoryInterface as CategoryEntity;
use MyHammer\Infrastructure\Entity\Doctrine\Category;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CategoryRepository extends ServiceEntityRepository implements CategoryInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * @param int $id
     * @return \MyHammer\Domain\Entity\CategoryInterface
     */
    public function findOneById(int $id): CategoryEntity
    {
        return $this->find($id);
    }
}
