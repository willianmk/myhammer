<?php

namespace MyHammer\Domain\Repository;

use MyHammer\Domain\Entity\CategoryInterface as CategoryEntity;
use MyHammer\Entity\Category;

interface CategoryInterface
{
    public function findOneById(int $id): CategoryEntity;
}