<?php

namespace MyHammer\Dto;

use MyHammer\Domain\Entity\RegionInterface as RegionEntity;

class Region
{
    private $zipCode;
    private $city;

    public function __construct(string $zipCode, string $city)
    {
        $this->zipCode = $zipCode;
        $this->city = $city;
    }

    public static function fromEntity(RegionEntity $entity)
    {
        return new static($entity->getZipCode(), $entity->getCity());
    }

    public function toArray(): array
    {
        return [
            'zipCode' => $this->zipCode,
            'city' => $this->city
        ];
    }
}
