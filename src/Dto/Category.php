<?php

namespace MyHammer\Dto;

use MyHammer\Domain\Entity\CategoryInterface;

class Category
{
    private $id;
    private $name;

    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public static function fromEntity(CategoryInterface $entity)
    {
        return new static($entity->getId(), $entity->getName());
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}