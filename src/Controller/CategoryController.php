<?php

namespace MyHammer\Controller;

use MyHammer\Domain\Repository\CategoryInterface;
use MyHammer\Dto\Category as CategoryDto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategoryController extends AbstractController
{
    private $repository;

    public function __construct(CategoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function search($id)
    {
        try {
            $category = $this->repository->findOneById($id);
        } catch (\Throwable $e) {
            return $this->json(
                [
                    'message' => 'Category not found: ' . $id,
                    'code' => 'CAT404'
                ],
                404
            );
        }

        $dto = CategoryDto::fromEntity($category);

        return $this->json(
            $dto->toArray()
        );
    }
}
