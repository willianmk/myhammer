<?php

namespace MyHammer\Tests\Domain\Dto;

use MyHammer\Domain\Entity\RegionInterface as RegionEntity;
use MyHammer\Dto\Region as RegionDto;
use MyHammer\Dto\Region;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class RegionTest extends TestCase
{
    public function test_will_create_region_from_entity()
    {
        $zipCode = '12345';
        $city = 'Berlin';

        $entity = $this->getMockBuilder(RegionEntity::class)
            ->getMock();

        $entity->expects($this->once())
            ->method('getZipCode')
            ->willReturn($zipCode);

        $entity->expects($this->once())
            ->method('getCity')
            ->willReturn($city);

        $dto = RegionDto::fromEntity($entity);

        $expected = [
            'zipCode' => '12345',
            'city' => 'Berlin'
        ];
        $result = $dto->toArray();

        $this->assertEquals($expected, $result);
    }
}
