<?php

namespace MyHammer\Domain\Entity;

interface JobInterface
{
    public function getId(): ?int;
    public function getCategoryId(): ?int;
    public function setCategoryId(?int $categoryId): self;
    public function getZipCode(): ?string;
    public function setZipCode(?string $zipCode): self;
    public function getTitle(): ?string;
    public function setTitle($title): self;
    public function getUser(): ?string;
    public function setUser(string $user): self;
    public function getDescription(): ?string;
    public function setDescription(string $description): self;
    public function getServiceDate(): ?\DateTimeImmutable;
    public function setServiceDate(\DateTimeImmutable $serviceDate): self;
    public function getCreatedAt(): ?\DateTimeInterface;
    public function setCreatedAt(\DateTimeInterface $createdAt): self;
}