<?php

namespace MyHammer\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @group functional
 */
class RegionControllerTest extends WebTestCase
{
    public function test_will_receive_region_by_zipCode()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            '/region/06895'
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('06895', $client->getResponse()->getContent());
    }

    public function test_will_receive_regoin_not_found()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            '/region/10000'
        );

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertContains('ZipCode not found', $client->getResponse()->getContent());
    }
}