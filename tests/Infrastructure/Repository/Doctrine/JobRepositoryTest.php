<?php

namespace MyHammer\Tests\Infrastructure\Repository\Doctrine;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use MyHammer\Dto\Job as JobDto;
use MyHammer\Infrastructure\Entity\Doctrine\Job;
use MyHammer\Infrastructure\Repository\Doctrine\JobRepository;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class JobRepositoryTest extends TestCase
{
    private $repository;
    private $entityManager;
    private $dto;

    private function setupRepositoryToCreateJob()
    {
        $this->dto = $this->createDto();
        $this->repository = $this->createRepository();

        $this->entityManager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['persist', 'flush'])
            ->getMock();

        $this->repository->expects($this->once())
            ->method('getEntityManager')
            ->will($this->returnValue($this->entityManager));

        $entity = new Job();

        $this->repository->expects($this->once())
            ->method('createEntity')
            ->with($this->dto)
            ->will($this->returnValue($entity));
    }

    private function createRepository()
    {
        return $this->getMockBuilder(JobRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['getEntityManager', 'createEntity', 'createQueryBuilder'])
            ->getMock();
    }

    private function createDto()
    {
        $categoryId = 123;
        $user = 'user.123';
        $title = 'Title test';
        $zipCode = '12345';
        $description = 'Description';
        $serviceDate = '2018-10-01';

        $dto = JobDto::fromArray(
            [
                'categoryId' => $categoryId,
                'user' => $user,
                'title' => $title,
                'zipCode' => $zipCode,
                'description' => $description,
                'serviceDate' => $serviceDate
            ]
        );

        return $dto;
    }

    /**
     * @param string $thrownException
     * @param string $expectedException
     * @param string $message
     * @dataProvider providerExceptionOnCreation
     */
    public function test_will_throw_exception_on_job_creation($thrownException, $expectedException, $message)
    {
        $this->setupRepositoryToCreateJob();

        $this->entityManager->expects($this->once())
            ->method('persist')
            ->will($this->throwException(new $thrownException()));

        $this->expectException($expectedException);
        $this->expectExceptionMessage($message);

        $this->repository->create($this->dto);
    }

    public function test_will_create_a_job()
    {
        $this->setupRepositoryToCreateJob();

        $this->entityManager->expects($this->once())
            ->method('persist');

        $this->entityManager->expects($this->once())
            ->method('flush');

        $this->repository->create($this->dto);
    }

    public function test_will_find_job_by_service_and_region()
    {
        $this->repository = $this->createRepository();
        $service = 'Service test';
        $region = 'Berlin';
        $user = 'test.user';

        $queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->setMethods(['select', 'innerJoin', 'where', 'andWhere', 'setParameter', 'getQuery'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->repository->expects($this->once())
            ->method('createQueryBuilder')
            ->with('j')
            ->will($this->returnValue($queryBuilder));

        $queryBuilder->expects($this->once())
            ->method('select');

        $queryBuilder->expects($this->exactly(2))
            ->method('innerJoin');

        $queryBuilder->expects($this->once())
            ->method('where')
            ->with('j.createdAt BETWEEN :lastMonth AND :today');

        $today = new \DateTimeImmutable();
        $lastMonth = $today->sub(new \DateInterval('P30D'));

        $queryBuilder->expects($this->exactly(5))
            ->method('setParameter')
            ->withConsecutive(
                [
                    'today', $today->format('Y-m-d 23:59:59')
                ],
                [
                    'lastMonth', $lastMonth->format('Y-m-d')
                ],
                [
                    'serviceName', $service
                ],
                [
                    'regionName', '%' . $region . '%'
                ],
                [
                    'userName', $user
                ]
            );

        $queryBuilder->expects($this->exactly(3))
            ->method('andWhere')
            ->withConsecutive(
                [
                    'c.name = :serviceName'
                ],
                [
                    'r.city LIKE :regionName'
                ],
                [
                    'j.user <> :userName'
                ]
            );

        $query = $this->getMockBuilder(AbstractQuery::class)
            ->disableOriginalConstructor()
            ->setMethods(['getResult', 'getSQL', '_doExecute'])
            ->getMock();

        $queryBuilder->expects($this->once())
            ->method('getQuery')
            ->will($this->returnValue($query));

        $query->expects($this->once())
            ->method('getResult')
            ->will($this->returnValue([]));

        $this->repository->findByServiceOrRegion($service, $region, $user);
    }

    public function providerExceptionOnCreation()
    {
        return [
            [
                'thrownException' => DBALException::class,
                'expectedException' => \RuntimeException::class,
                'message' => 'Error on constraints'
            ],
            [
                'thrownException' => ORMException::class,
                'expectedException' => \InvalidArgumentException::class,
                'message' => 'Error in database'
            ]
        ];
    }
}
