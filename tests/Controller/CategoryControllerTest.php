<?php

namespace MyHammer\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @group functional
 */
class CategoryControllerTest extends WebTestCase
{
    public function test_will_get_OK_request()
    {
        $client = static::createClient();
        $client->request('GET', '/category/804040');

        $expected = '804040';

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains($expected, $client->getResponse()->getContent());
    }

    public function test_will_get_Not_Found_status_code()
    {
        $client = static::createClient();
        $client->request('GET', '/category/9999');

        $expected = 'CAT404';

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertContains($expected, $client->getResponse()->getContent());
    }
}