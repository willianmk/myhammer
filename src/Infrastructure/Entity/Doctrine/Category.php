<?php

namespace MyHammer\Infrastructure\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use MyHammer\Domain\Entity\CategoryInterface;

/**
 * @ORM\Entity(repositoryClass="MyHammer\Infrastructure\Repository\Doctrine\CategoryRepository")
 */
class Category implements CategoryInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): CategoryInterface
    {
        $this->name = $name;
        return $this;
    }
}
