<?php

namespace MyHammer\Tests\Domain\Dto;

use MyHammer\Domain\Entity\CategoryInterface;
use MyHammer\Dto\Category as CategoryDto;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class CategoryTest extends TestCase
{
    public function test_will_create_from_entity()
    {
        $id = 111;
        $name = 'Name test';

        $entity = $this->getMockBuilder(CategoryInterface::class)
            ->setMethods(['getId', 'getName', 'setName'])
            ->getMock();

        $entity->expects($this->once())
            ->method('getId')
            ->will($this->returnValue($id));

        $entity->expects($this->once())
            ->method('getName')
            ->will($this->returnValue($name));

        $dto = CategoryDto::fromEntity(
            $entity
        );

        $result = $dto->toArray();
        $expected = [
            'id' => $id,
            'name' => $name
        ];

        $this->assertEquals($expected, $result);
    }
}
