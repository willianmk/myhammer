-- Category table default data

INSERT INTO `myhammer`.`category` (`id`, `name`) VALUES ('804040', 'Sonstige Umzugsleistungen');
INSERT INTO `myhammer`.`category` (`id`, `name`) VALUES ('802030', 'Abtransport, Entsorgung und Entr�mpelung');
INSERT INTO `myhammer`.`category` (`id`, `name`) VALUES ('411070', 'Fensterreinigung');
INSERT INTO `myhammer`.`category` (`id`, `name`) VALUES ('402020', 'Holzdielen schleifen');
INSERT INTO `myhammer`.`category` (`id`, `name`) VALUES ('108140', 'Kellersanierung');

-- Zip default data
INSERT INTO `myhammer`.`region` (`zip_code`, `city`) VALUES ('10115', 'Berlin');
INSERT INTO `myhammer`.`region` (`zip_code`, `city`) VALUES ('32457', 'Porta Westfalica');
INSERT INTO `myhammer`.`region` (`zip_code`, `city`) VALUES ('01623', 'Lommatzsch');
INSERT INTO `myhammer`.`region` (`zip_code`, `city`) VALUES ('21521', 'Hamburg');
INSERT INTO `myhammer`.`region` (`zip_code`, `city`) VALUES ('06895', 'B�lzig');
INSERT INTO `myhammer`.`region` (`zip_code`, `city`) VALUES ('01612', 'Diesbar-Seu�litz');
