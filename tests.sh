#!/bin/bash
option=$1

case "$option" in
    all)
        parameters=""
        ;;
    all-coverage)
        parameters="--coverage-html=./build/coverage/html"
        ;;
    unit)
        parameters="--group=unit"
        ;;
    functional)
        parameters="--group=functional"
        ;;
    *)
        echo "Invalid parameter"
        echo "Usage: $0 {all|all-coverage|unit|functional}"
        exit 1
esac

docker-compose -f ./docker/docker-compose.yml down
docker-compose -f ./docker/docker-compose.test.yml up -d

echo "Waiting until mysql is properly created..."
sleep 10 # This was necessary because MySQL takes time to populate initial data

docker-compose -f ./docker/docker-compose.test.yml exec php.test ./bin/phpunit $parameters

docker-compose -f docker/docker-compose.test.yml down
docker-compose -f ./docker/docker-compose.yml up -d