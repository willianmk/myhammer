<?php
/**
 * Created by PhpStorm.
 * User: willian.kumatsu
 * Date: 06.11.18
 * Time: 10:24
 */

namespace MyHammer\Tests\Infrastructure\Repository\Doctrine;

use MyHammer\Domain\Entity\CategoryInterface;
use MyHammer\Infrastructure\Entity\Doctrine\Category as CategoryEntity;
use MyHammer\Infrastructure\Repository\Doctrine\CategoryRepository;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class CategoryRepositoryTest extends TestCase
{
    private $repository;

    protected function setUp()
    {
        parent::setUp();

        $this->repository = $this->getMockBuilder(CategoryRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['find'])
            ->getMock();
    }

    /**
     * @expectedException \Throwable
     */
    public function test_will_throw_exception_on_category_find()
    {
        $id = 999;

        $this->repository->expects($this->once())
            ->method('find')
            ->will($this->returnValue(null));

        $this->repository->findOneById($id);
    }

    public function test_will_retrieve_entity_on_category_find()
    {
        $id = 100;

        $entity = new CategoryEntity();
        $this->repository->expects($this->once())
            ->method('find')
            ->with($id)
            ->will($this->returnValue($entity));

        $result = $this->repository->findOneById($id);
        $this->assertInstanceOf(CategoryInterface::class, $result);
    }
}
