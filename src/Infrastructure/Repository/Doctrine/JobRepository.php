<?php

namespace MyHammer\Infrastructure\Repository\Doctrine;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use MyHammer\Domain\Repository\JobInterface;
use MyHammer\Dto\Job as JobDto;
use MyHammer\Infrastructure\Entity\Doctrine\Category;
use MyHammer\Infrastructure\Entity\Doctrine\Job as JobEntity;
use MyHammer\Infrastructure\Entity\Doctrine\Job;
use MyHammer\Infrastructure\Entity\Doctrine\Region;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class JobRepository extends ServiceEntityRepository implements JobInterface
{
    private $validator;

    public function __construct(RegistryInterface $registry, ValidatorInterface $validator)
    {
        parent::__construct($registry, JobEntity::class);
        $this->validator = $validator;
    }

    /**
     * @param \MyHammer\Dto\Job $dto
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function create(JobDto $dto)
    {
        try{
            $entity = $this->createEntity($dto);

            $entityManager = $this->getEntityManager();
            $entityManager->persist($entity);
            $entityManager->flush();
        } catch (DBALException $e) {
            throw new \RuntimeException('Error on constraints');
        } catch (ORMException $e) {
            throw new \InvalidArgumentException('Error in database');
        }
    }

    /**
     * @param string $serviceName
     * @param string $regionName
     * @param string $user
     * @return mixed
     * @throws \Exception
     */
    public function findByServiceOrRegion(string $serviceName, string $regionName, string $user): array
    {
        $today = new \DateTimeImmutable();
        $lastMonth = $today->sub(new \DateInterval('P30D'));

        $query = $this->createQueryBuilder('j');
        $query->select(['j']);

        $query->innerJoin(Category::class, 'c', Join::WITH, 'j.categoryId = c.id');
        $query->innerJoin(Region::class, 'r', Join::WITH, 'j.zipCode = r.zipCode');
        $query->where('j.createdAt BETWEEN :lastMonth AND :today');

        $query->setParameter('today', $today->format('Y-m-d 23:59:59'));
        $query->setParameter('lastMonth', $lastMonth->format('Y-m-d'));

        if ($serviceName) {
            $query->andWhere('c.name = :serviceName');
            $query->setParameter('serviceName', $serviceName);
        }

        if ($regionName) {
            $query->andWhere('r.city LIKE :regionName');
            $query->setParameter('regionName', '%' . $regionName . '%');
        }

        $query->andWhere('j.user <> :userName');
        $query->setParameter('userName', $user);

        return $query->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    /**
     * @param \Symfony\Component\Validator\ConstraintViolationListInterface $errors
     * @return string
     */
    private function formatMessages(ConstraintViolationListInterface $errors): string
    {
        $messages = [];
        foreach ($errors as $error) {
            $messages[] = $error->getMessage();
        }

        return json_encode($messages);
    }

    /**
     * @param \MyHammer\Dto\Job $dto
     * @return \MyHammer\Infrastructure\Entity\Doctrine\Job
     */
    protected function createEntity(JobDto $dto): JobEntity
    {
        $entity = new JobEntity();
        $entity->setCategoryId($dto->getCategoryId());
        $entity->setUser($dto->getUser());
        $entity->setZipCode($dto->getZipCode());
        $entity->setTitle($dto->getTitle());
        $entity->setDescription($dto->getDescription());
        $entity->setServiceDate($dto->getServiceDate());

        $errors = $this->validator->validate($entity);
        if (count($errors) > 0) {
            throw new \InvalidArgumentException($this->formatMessages($errors));
        }

        return $entity;
    }
}
