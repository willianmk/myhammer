<?php

namespace MyHammer\Tests\Domain\Dto;

use MyHammer\Domain\Entity\JobInterface as JobEntity;
use MyHammer\Dto\Job as JobDto;
use MyHammer\Dto\Job;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class JobTest extends TestCase
{
    public function test_will_create_from_entity()
    {
        $categoryId = 111;
        $user = 'user.test';
        $title = 'Title Test';
        $zipCode = '12345';
        $description = 'Description test';
        $serviceDate = \DateTimeImmutable::createFromFormat('Y-m-d', '2018-10-01');

        $entity = $this->getMockBuilder(JobEntity::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entity->expects($this->once())
            ->method('getCategoryId')
            ->willReturn($categoryId);

        $entity->expects($this->once())
            ->method('getUser')
            ->willReturn($user);

        $entity->expects($this->once())
            ->method('getTitle')
            ->willReturn($title);

        $entity->expects($this->once())
            ->method('getZipCode')
            ->willReturn($zipCode);

        $entity->expects($this->once())
            ->method('getDescription')
            ->willReturn($description);

        $entity->expects($this->once())
            ->method('getServiceDate')
            ->willReturn($serviceDate);

        $dto = JobDto::fromEntity($entity);

        $result = $dto->toArray();

        $expected = [
            'categoryId' => $categoryId,
            'user' => $user,
            'title' => $title,
            'zipCode' => $zipCode,
            'description' => $description,
            'serviceDate' => $serviceDate->format('Y-m-d')
        ];

        $this->assertEquals($expected, $result);
    }

    public function test_will_create_job_dto_from_array_successfully()
    {
        $categoryId = 111;
        $user = 'user.test';
        $title = 'Title Test';
        $zipCode = '12345';
        $description = 'Description test';
        $serviceDate = '2018-10-01';

        $dto = JobDto::fromArray(
            [
                'categoryId' => $categoryId,
                'user' => $user,
                'title' => $title,
                'zipCode' => $zipCode,
                'description' => $description,
                'serviceDate' => $serviceDate
            ]
        );

        $expected = [
            'categoryId' => $categoryId,
            'user' => $user,
            'title' => $title,
            'zipCode' => $zipCode,
            'description' => $description,
            'serviceDate' => $serviceDate
        ];

        $result = $dto->toArray();

        $this->assertEquals($expected, $result);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function test_will_throw_exception_with_invalid_date_format_on_job_dto_creation()
    {
        $categoryId = 111;
        $user = 'user.test';
        $title = 'Title Test';
        $zipCode = '12345';
        $description = 'Description test';
        $serviceDate = '2018-10';

        $dto = Job::fromArray(
            [
                'categoryId' => $categoryId,
                'user' => $user,
                'title' => $title,
                'zipCode' => $zipCode,
                'description' => $description,
                'serviceDate' => $serviceDate
            ]
        );
    }
}
