<?php

namespace MyHammer\Domain\Repository;

use MyHammer\Dto\Job;

interface JobInterface
{
    public function create(Job $job);
    public function findByServiceOrRegion(string $serviceName, string $regionName, string $user): array ;
}