<?php

namespace MyHammer\Domain\Entity;

interface CategoryInterface
{
    public function getId(): ?int;
    public function getName(): ?string;
    public function setName(string $name): self;
}
