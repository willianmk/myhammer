<?php

namespace MyHammer\Tests\Infrastructure\Repository\Doctrine;

use MyHammer\Domain\Entity\RegionInterface;
use MyHammer\Infrastructure\Entity\Doctrine\Region as RegionEntity;
use MyHammer\Infrastructure\Repository\Doctrine\RegionRepository;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class RegionRepositoryTest extends TestCase
{
    private $repository;

    protected function setUp()
    {
        parent::setUp();

        $this->repository = $this->getMockBuilder(RegionRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findOneBy'])
            ->getMock();
    }

    /**
     * @expectedException \Throwable
     */
    public function test_will_throw_exception_on_region_not_found()
    {
        $zipCode = '12012';

        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->will($this->returnValue(null));

        $this->repository->findByZipCode($zipCode);
    }

    public function test_will_return_entity_finding_region()
    {
        $zipCode = '12012';

        $entity = new RegionEntity();

        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->with(['zipCode' => $zipCode])
            ->will($this->returnValue($entity));

        $result = $this->repository->findByZipCode($zipCode);
        $this->assertInstanceOf(RegionInterface::class, $result);
    }
}
