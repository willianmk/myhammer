<?php

namespace MyHammer\Infrastructure\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints;
use MyHammer\Domain\Entity\JobInterface;

/**
 * @ORM\Entity(repositoryClass="MyHammer\Infrastructure\Repository\Doctrine\JobRepository")
 */
class Job implements JobInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $categoryId;

    /**
     * @ORM\Column(type="integer")
     * @Constraints\Length(min=5, max=5, exactMessage="job.zipCode")
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=50)
     * @Constraints\Length(min=5, max=50, minMessage="job.title", maxMessage="job.title")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=50)
     * @Constraints\Length(min=3, max=50, minMessage="job.user", maxMessage="job.user")
     */
    private $user;

    /**
     * @ORM\Column(type="text")
     * @Constraints\Length(min=10), minMessage="job.description"
     */
    private $description;

    /**
     * @ORM\Column(type="date_immutable")
     * @Constraints\Date(message="job.serviceDate")
     */
    private $serviceDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    public function setCategoryId(?int $categoryId): JobInterface
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): JobInterface
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle($title): JobInterface
    {
        $this->title = $title;
        return $this;
    }

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(string $user): JobInterface
    {
        $this->user = $user;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): JobInterface
    {
        $this->description = $description;
        return $this;
    }

    public function getServiceDate(): ?\DateTimeImmutable
    {
        return $this->serviceDate;
    }

    public function setServiceDate(\DateTimeImmutable $serviceDate): JobInterface
    {
        $this->serviceDate = $serviceDate;
        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): JobInterface
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
