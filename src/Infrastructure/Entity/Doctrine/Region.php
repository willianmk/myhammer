<?php

namespace MyHammer\Infrastructure\Entity\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use MyHammer\Domain\Entity\RegionInterface;

/**
 * @ORM\Entity(repositoryClass="MyHammer\Infrastructure\Repository\Doctrine\RegionRepository")
 */
class Region implements RegionInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=5)
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $city;

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): RegionInterface
    {
        $this->city = $city;
        return $this;
    }
}
