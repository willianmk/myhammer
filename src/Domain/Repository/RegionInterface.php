<?php

namespace MyHammer\Domain\Repository;

use MyHammer\Domain\Entity\RegionInterface as RegionEntity;

interface RegionInterface
{
    public function findByZipCode(string $zipCode): RegionEntity;
}