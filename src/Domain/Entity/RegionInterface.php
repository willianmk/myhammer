<?php

namespace MyHammer\Domain\Entity;

interface RegionInterface
{
    public function getZipCode(): ?string;
    public function getCity(): ?string;
    public function setCity(string $city): self;
}