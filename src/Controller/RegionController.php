<?php

namespace MyHammer\Controller;

use MyHammer\Domain\Repository\RegionInterface;
use MyHammer\Dto\Region as RegionDto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RegionController extends AbstractController
{
    private $repository;

    public function __construct(RegionInterface $repository)
    {
        $this->repository = $repository;
    }

    public function search($zipCode)
    {
        try{
            $region = $this->repository->findByZipCode($zipCode);
        } catch (\Throwable $e) {
            return $this->json(
                [
                    'message' => 'ZipCode not found: ' . $zipCode,
                    'code' => 'REG404'
                ],
                404
            );
        }

        $dto = RegionDto::fromEntity($region);
        return $this->json($dto->toArray());
    }
}
