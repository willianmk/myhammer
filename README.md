MyHammer coding challenge
=========================

This project was built for MyHammer coding challenge. This documentation aims specifying how what this project contains.

*Requirements:*

To run this project, you should have the following programs:

* docker
* docker-compose

*What will be installed*

In the file `docker/docker-compose.yml` there is the specification of the stack used, that are:

* PHP:7.2-fpm
  * Extensions: zip, pdo, pdo_mysql, xdebug
  * Composer
  * Symfony 4.1
    * Browser-kit
    * Maker-bundle
    * ORM
    * Test-pack
  * PHPUnit  
* MySQL:5.7
* Nginx:latest

*How to run:*

To simplify the process, I created a shell script to setup and up the application: `./run.sh`. It will run internally
 a docker-compose up and install the dependencies.
 
To run the tests, you may run `./tests.sh` with the following parameters:

* all: It will run the entire test suite
* all-coverage: Entire suite with coverage. Coverage will be on `./build/` directory
* unit: It will run the tests on group `unit`
* functional: It will run the tests on the group `functional`

You can kill the application with `docker-compose -f docker/docker-compose.yml down`

*Note: With this script, the application will be stopped because of container conflicts. This command is needed to 
run all the test suite*

End-points are the following:
* GET /category/{id}: receives a Category object the ID
* GET /region/{zipCode}: receives a Region object searching for the zipCode
* GET /jobs: receives a collection of Jobs within 30 days including today. It may be filtered by zipCode or service 
as query string
* POST /job: creates a job, based on a Json representation of Job object

An openAPI-style documentation is placed on `Docs/swagger.api.yml`.

For manual tests, there is a Postman file that can me imported to tests the requests.

*Note: This service is running on the port `8000` accessing from outside the container. It may conflict with other 
service on the same port*