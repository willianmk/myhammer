<?php

namespace MyHammer\Dto;

use DateTimeImmutable;
use MyHammer\Domain\Entity\JobInterface as JobEntity;

class Job
{
    private $categoryId;
    private $user;
    private $title;
    private $zipCode;
    private $description;
    private $serviceDate;

    public function __construct(
        int $categoryId,
        string $user,
        string $title,
        int $zipCode,
        string $description,
        DateTimeImmutable $serviceDate
    ) {
        $this->categoryId = $categoryId;
        $this->user = $user;
        $this->title = $title;
        $this->zipCode = $zipCode;
        $this->description = $description;
        $this->serviceDate = $serviceDate;
    }

    public static function fromArray(array $data)
    {
        $categoryId = $data['categoryId'] ?? 0;
        $user = $data['user'] ?? '';
        $title = $data['title'] ?? '';
        $zipCode = $data['zipCode'] ?? 0;
        $description = $data['description'] ?? '';
        $serviceDate = DateTimeImmutable::createFromFormat('Y-m-d', $data['serviceDate']);

        if ($serviceDate === false) {
            throw new \InvalidArgumentException(json_encode(['Invalid date format']));
        }

        return new static($categoryId, $user, $title, $zipCode, $description, $serviceDate);
    }

    public static function fromEntity(JobEntity $entity)
    {
        return new static(
            $entity->getCategoryId(),
            $entity->getUser(),
            $entity->getTitle(),
            $entity->getZipCode(),
            $entity->getDescription(),
            $entity->getServiceDate()
        );
    }

    public function toArray()
    {
        return [
            'categoryId' => $this->getCategoryId(),
            'user' => $this->getUser(),
            'title' => $this->getTitle(),
            'zipCode' => $this->getZipCode(),
            'description' => $this->getDescription(),
            'serviceDate' => $this->getServiceDate()->format('Y-m-d')
        ];
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getZipCode(): int
    {
        return $this->zipCode;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getServiceDate(): \DateTimeImmutable
    {
        return $this->serviceDate;
    }
}
