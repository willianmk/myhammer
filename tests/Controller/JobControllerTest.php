<?php

namespace MyHammer\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @group functional
 */
class JobControllerTest extends WebTestCase
{
    private $client;

    protected function setUp()
    {
        parent::setUp();
        $this->client = static::createClient();
        $container = self::$container;

        $entityManager = $container->get('doctrine')->getManager();

        $connection = $entityManager->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        $q = $dbPlatform->getTruncateTableSql('job');
        $connection->executeUpdate($q);
        $connection->query('SET FOREIGN_KEY_CHECKS=1');
    }

    public function test_will_create_new_job()
    {
        $this->client->request(
            'POST',
            '/job',
            [],
            [],
            [
                'CONTENT_TYPE' => 'Application/json'
            ],
            '
{
	"job": {
		"categoryId": 402020,
		"user": "test.user",
		"zipCode": 10115,
		"title": "Title test",
		"description": "Description about the test",
		"serviceDate": "2018-10-01"
	}
}'
        );

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
        $this->assertContains('402020', $this->client->getResponse()->getContent());
    }

    public function test_will_receive_server_internal_server_error_on_invalid_category_fk()
    {
        $this->client->request(
            'POST', '/job', [], [], ['CONTENT_TYPE' => 'Application/json'],
            '
{
	"job": {
		"categoryId": 40202,
		"user": "test.user",
		"zipCode": 10115,
		"title": "Title test",
		"description": "Description about the test",
		"serviceDate": "2018-10-01"
	}
}           
            '
        );

        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Error on constraints', $this->client->getResponse()->getContent());
    }

    public function test_will_receive_bad_request_error_on_invalid_payload()
    {
        $this->client->request(
            'POST', '/job', [], [], ['CONTENT_TYPE' => 'Application/json'],
            '
{
	"job": {
		"categoryId": 40202,
		"user": "test.user",
		"zipCode": 1011,
		"title": "Title test",
		"description": "Description about the test",
		"serviceDate": "2018-10-01"
	}
}           
            '
        );

        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
        $this->assertContains('job.zipCode', $this->client->getResponse()->getContent());
    }

    public function test_will_list_jobs_up_to_30_days_before()
    {
        $this->client->request(
            'POST',
            '/job',
            [],
            [],
            ['CONTENT_TYPE' => 'Application/json'],
            '
{
	"job": {
		"categoryId": 402020,
		"user": "test.user",
		"zipCode": 10115,
		"title": "Title test",
		"description": "Description about the test",
		"serviceDate": "2018-01-01"
	}
}');
        $this->client->request(
            'GET',
            '/jobs',
            [
                'region' => 'Berlin'
            ],
            [],
            [
                'X-User' => 'test.not.user'
            ]
        );

        $this->client->followRedirect();

        $content = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertCount(1, $content);
    }

    public function test_will_not_receive_a_job()
    {
        $this->client->request(
            'GET',
            '/jobs',
            [
                'region' => 'Berlin'
            ],
            [],
            [
                'X-User' => 'test.not.user'
            ]
        );

        $this->client->followRedirect();

        $content = json_decode($this->client->getResponse()->getContent(), true);
        $message = 'Jobs not found in 30 days';

        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertContains($message, $content);
    }
}