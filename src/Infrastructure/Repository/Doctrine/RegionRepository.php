<?php

namespace MyHammer\Infrastructure\Repository\Doctrine;

use MyHammer\Domain\Entity\RegionInterface as EntityInterface;
use MyHammer\Domain\Repository\RegionInterface;
use MyHammer\Infrastructure\Entity\Doctrine\Region;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class RegionRepository extends ServiceEntityRepository implements RegionInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Region::class);
    }

    public function findByZipCode(string $zipCode): EntityInterface
    {
        return $this->findOneBy(['zipCode' => $zipCode]);
    }
}
