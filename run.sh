#!/bin/bash
cp ./.env.dist ./.env
docker-compose -f docker/docker-compose.yml up -d --build
docker-compose -f docker/docker-compose.yml exec php composer install
